

+ [《learn-ffmpeg》](https://github.com/feixiao/learn-ffmpeg)  学习音视频知识，整理资料，编写技术手册。
+ [《ffplay分析》](https://github.com/feixiao/ffsrc)
+ [《FFmpeg 自定义 IO 操作之 AVIO 解析》](https://octalzero.com/article/17802173-7d46-4be2-be66-46c5a1824724)
+ [《FFmpeg源码分析》](https://lazybing.github.io/blog/categories/ffmpegyuan-ma-fen-xi/)